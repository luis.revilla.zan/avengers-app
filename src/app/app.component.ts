import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Thanos Snapper';
  public characters = [
    { name: "Ant-Man", affiliation: 0, img: "antman", isSnapped: false },
    { name: "Black Panther", affiliation: 0, img: "blackpanther", isSnapped: false },
    { name: "Black Widow", affiliation: 0, img: "blackwidow", isSnapped: false },
    { name: "Captain America", affiliation: 0, img: "captainamerica", isSnapped: false },
    { name: "Captain Marvel", affiliation: 0, img: "captainmarvel", isSnapped: false },
    { name: "Doctor Strange", affiliation: 0, img: "drstrange", isSnapped: false },
    { name: "Drax the Destroyer", affiliation: 1, img: "drax", isSnapped: false },
    { name: "Falcon", affiliation: 0, img: "falcon", isSnapped: false },
    { name: "Gamora", affiliation: 1, img: "gamora", isSnapped: false },
    { name: "Groot", affiliation: 1, img: "groot", isSnapped: false },
    { name: "Hawkeye", affiliation: 0, img: "hawkeye", isSnapped: false },
    { name: "Hulk", affiliation: 0, img: "hulk", isSnapped: false },
    { name: "Iron Man", affiliation: 0, img: "ironman", isSnapped: false },
    { name: "Mantis", affiliation: 1, img: "mantis", isSnapped: false },
    { name: "Nebula", affiliation: 1, img: "nebula", isSnapped: false },
    { name: "Nick Fury", affiliation: 0, img: "nickfury", isSnapped: false },
    { name: "Pepper Potts", affiliation: 0, img: "pepperpotts", isSnapped: false },
    { name: "Rocket Raccoon", affiliation: 1, img: "rocket", isSnapped: false },
    { name: "Scarlet Witch", affiliation: 0, img: "scarletwitch", isSnapped: false },
    { name: "Spider-Man", affiliation: 0, img: "spiderman", isSnapped: false },
    { name: "Star-Lord", affiliation: 1, img: "starlord", isSnapped: false },
    { name: "Thor", affiliation: 0, img: "thor", isSnapped: false },
    { name: "War Machine", affiliation: 0, img: "warmachine", isSnapped: false },
    { name: "Wasp", affiliation: 0, img: "wasp", isSnapped: false },
    { name: "Winter Soldier", affiliation: 0, img: "wintersoldier", isSnapped: false }
  ];
  public unsnappedCharacters = []
  public snappedCharacters = []

  constructor() { }

  ngOnInit() {
    this.unsnappedCharacters = this.getUnsnappedCharacters();
  }

  snap = async () => {
    let snappingIndexes = [];
    while (snappingIndexes.length < this.unsnappedCharacters.length / 2) {
      const newIndex = Math.floor(Math.random() * this.unsnappedCharacters.length);
      if (!snappingIndexes.includes(newIndex)) snappingIndexes.push(newIndex);
    }
    const snappingCharacters = this.unsnappedCharacters.filter((character, index) => snappingIndexes.includes(index))
    this.shuffle(snappingCharacters);
    for (const character of snappingCharacters) {
      character.isSnapped = true;
      await this.sleep(2000);
    }
    this.unsnappedCharacters = this.getUnsnappedCharacters();
    this.snappedCharacters = this.getSnappedCharacters()
  }

  unsnap = async () => {
    for (const character of this.snappedCharacters) {
      character.isSnapped = false;
      await this.sleep(1000);
    }
    this.unsnappedCharacters = this.getUnsnappedCharacters();
    this.snappedCharacters = this.getSnappedCharacters()
  }

  getUnsnappedCharacters = () => this.characters.filter(character => !character.isSnapped);
  getSnappedCharacters = () => this.characters.filter(character => character.isSnapped);

  sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

  shuffle = array => array.sort(() => Math.random() - 0.5);

}
