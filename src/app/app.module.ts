import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { ParticleEffectButtonModule } from 'angular-particle-effect-button';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ParticleEffectButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
